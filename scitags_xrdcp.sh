#!/bin/bash
# SuperComputing Scitags demo
# Author: Marian Babik

# path to ssh key (to access remote DTNs)
SSHKEY=<ssh_key_path>
DTN_FILE=$1
# IP6 of the showfloor DTN
SC_DTN_IP=<sc_dtn_ip>
IPERF_SRV='5201 5202 5203 5204 5205 5206'
IPERF_TIMEOUT=15
XRDCP_TIMEOUT=15

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

function run_iperf3 () {
    echo "Running iperf3s ..."
    while read -r DTN_USER DTN_HOST IPERF_LIMIT IPERF_PORT IPERF_SIZE IPERF_FLAGS XROOT_LIMIT XROOT_PATH SSH_EXTRA_ARGS; do
        timeout ${IPERF_TIMEOUT}m ssh $SSH_EXTRA_ARGS $DTN_USER@$DTN_HOST "iperf3 $IPERF_FLAGS -b $IPERF_LIMIT -n $IPERF_SIZE -i 1 -c $SC_DTN_IP -p $IPERF_PORT" > out/$DTN_HOST.out 2>&1 &
        ssh_pids[${i}]=$!
    done < $DTN_FILE

    echo "Waiting for iperfs to finish ..."
    wait "${ssh_pids[@]}"
}

function run_xrdcp () {
    echo "Running xrdcps ..."
    while read -r DTN_USER DTN_HOST IPERF_LIMIT IPERF_PORT IPERF_SIZE IPERF_FLAGS XROOT_LIMIT XROOT_PATH SSH_EXTRA_ARGS; do
        timeout ${XRDCP_TIMEOUT}m ssh $SSH_EXTRA_ARGS $DTN_USER@$DTN_HOST "curl -g http://[$XROOT_LIMIT]:1094/$XROOT_PATH --output /dev/null" > out/$DTN_HOST.out 2>&1 &
    done < $DTN_FILE

    echo "Waiting for xrdcps to finish ..."
    wait
}

mkdir -p out

session_time=$(date +%M)

sudo systemctl -q stop flowd@xrootd
sudo systemctl -q stop flowd@iperf

echo "Starting flowd@xrootd ..."
sudo systemctl start flowd@xrootd
sudo systemctl status flowd@xrootd
sleep 15

while true
do
    echo "Session start ..."
    run_xrdcp
done
