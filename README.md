# Supercomputing Demo 

This repository summarises basic setup for the following SC demos:

- Memory-to-memory transfers using iperf3 with packet marking (via eBPF-TC) and flow labelling (fireflies) using flowd service 
- Disk-to-disk transfers using XRootD with packet marking (via eBPF-TC) and flow labelling (fireflies) using flowd service


## Requirements
*XRootD* server and clients:
* Ubuntu packages are available from certain version (https://packages.ubuntu.com/search?suite=default&section=all&arch=any&keywords=xrootd&searchon=names)

*Flowd* service
* Ubuntu packages are not available, but there are two options to install
* Pypi https://pypi.org/project/flowd/ (pip install flowd)
* Run in docker container: https://github.com/scitags/flowd#installation
* Flowd code is at: https://github.com/scitags/flowd
* Packages for RHEL8, RHEL9 (or compatible) are in the works 

DTN must have IPv6 configured and working since packet marking relies on IPv6.

## Configuration
* XRootD server configuration:
```config
marian@r7503:~/SC22$ cat /etc/xrootd/xrootd-standalone.cfg
# rootdir should point to SSD RAID to get better performance
set rootdir = /data
all.export /
# xrd.trace all -debug
# xrd.trace all

# this line is important for xrootd to correctly determine its network
# especially in multi-NIC environemnt  
xrd.network routes split use management,vlan2026

if exec xrootd
xrd.protocol http /usr/lib/x86_64-linux-gnu/libXrdHttp-5.so
fi

xrootd.pmark defsfile wget http://api.scitags.org
# replace <scitags_collector> with public IP address of the localhost 
xrootd.pmark ffdest <scitags_collector>:10514
xrootd.pmark domain any
xrootd.pmark debug
xrootd.pmark trace

# the following is optional and only used for default/fallback mappings
xrootd.pmark map2exp path /data/atlas atlas
xrootd.pmark map2exp path /atlas atlas
xrootd.pmark map2exp path /data/cms cms
xrootd.pmark map2exp path /cms cms
xrootd.pmark map2exp path /data/lhcb lhcb
xrootd.pmark map2exp path /lhcb lhcb
xrootd.pmark map2exp path /data/alice alice
xrootd.pmark map2exp path /alice alice
xrootd.pmark map2exp default atlas
xrootd.pmark map2act cms default default
xrootd.pmark map2act atlas default default
xrootd.pmark map2act lhcb default default
xrootd.pmark map2act alice default default
```
XRootd will require /data partition which is going to host the files (must be owned by user/group xrootd/xrootd). Sample files can be created via:
```
sudo fallocate -l 800G big.img
sudo fallocate -l 400G medium.img
```

Flowd has two configurations, one for xrootd and the other for iperf3. 
Flowd xrootd config:
```
marian@r7503:~/SC22$ cat /etc/flowd/flowd-xrootd.cfg
PLUGIN='firefly'
BACKEND='ebpf,prometheus,udp_firefly'
NETWORK_INTERFACE='vlan2026,vlan3301' 
FLOW_MAP_API='http://scitags.github.io/api_n.json'
PROMETHEUS_SRV_PORT=9010
PROMETHEUS_SS_PATH='/usr/bin/ss'
```
Flowd iperf3 config:
```
marian@r7503:~/SC22$ cat /etc/flowd/flowd-iperf.cfg
PLUGIN='iperf_rr'
BACKEND='ebpf,prometheus,udp_firefly'
NETWORK_INTERFACE='vlan2026,vlan3301' 
FLOW_MAP_API='http://scitags.github.io/api_n.json'
PROMETHEUS_SRV_PORT=9010
PROMETHEUS_SS_PATH='/usr/bin/ss'
```
In both cases NETWORK_INTERFACE needs to point to a comma separated list of network interface(s) that will mark the packets. The following backends are supported: ebpf (for packet marking/changing IPv6 flow label of each packet), udp_firefly (for flow labelling/sending fireflies), prometheus (to export flows via prometheus client). They can be enabled/disabled by editing the BACKEND setting.

## Services
XRootD can be started and followed up via systemd/log file, e.g.
```shell
$ sudo systemctl start xrootd@standalone
$ sudo systemctl status xrootd@standalone
$ less +F /var/log/xrootd/standalone/xrootd.log
```

With native install (via pip) flowd can be started and followed up via systemd/journal, e.g.
```shell
$ sudo systemctl enable flowd@xrootd
$ sudo systemctl enable flowd@iperf
$ sudo systemctl start flowd@xrootd
$ sudo systemctl stop flowd@iperf
$ sudo systemctl start flowd@xrootd
$ sudo systemctl stop flowd@iperf
$ sudo journalctl -u flowd@xrootd -n 100 -f
$ sudo journalctl -u flowd@iperf -n 100 -f
```
Note that only one instance of flowd can be running, so either flowd@xrootd or flowd@iperf, not both at the same time.
With docker instance, flowd will accept configuration file via argument and will report debug log via docker (container must be privileged and use host networking).

## XRootD Tests
XRootd and Flowd needs to be running:
```shell
$ sudo systemctl status xrootd@standalone
$ sudo systemctl status flowd@xrootd
```

Client will initiate transfer and will pass scitag atttribute to indicate experiment/activity IDs, e.g.
```shell
$ curl http://[fe80::ac0:ebff:fe1c:b0d2%vlan3301]:1094//data/big.img?scitag.flow=330 --output test.img
```

scitag.flow attribute indicates the experiment and activity IDs that will be used in the IPv6 flow label (for packet marking) or UDP firefly (for flow labelling), it's determined using the following formula:
```
scitag.flow=<experimentID><<6|<activityID> 
where experimentID and activityID are taken from http://scitags.github.io/api.json
```

## Iperf3 Tests
Flowd and iperf3 server(s) needs to be running:
```shell
$ sudo systemctl status flowd@iperf
$ iperf3 -s [-p 5201-N]
```

Client will initiate iperf3 transfer, experiment/activity IDs allocation will be determined by the iperf flowd plugin (round robin from fixed set), e.g.
```shell
iperf3 -c <server_ip6>%vlan
```

## Demo

The following two scripts can be used to start high-performance demos: 
* scitags_xrdcp.sh - runs xrootd transfers
* scitags_iperf.sh - runs iperf 

Both will establish ssh sessions to remote DTNs and execute xrdcp or iperf to start transferring data from/to the showfloor DTN. They accept as first argument a DTN file which has a list of DTNs and parameters with the following format:
```
DTN_USER DTN_HOST IPERF_LIMIT IPERF_PORT IPERF_SIZE IPERF_FLAGS XROOT_LIMIT XROOT_PATH SSH_EXTRA_ARGS
for scitags_iperf.sh the file would look something like this:
<user> <dtn_ssh_ip> 0 5201 1000G -RZ not_used not_used -4
<user2> <dtn_ssh_ip2> 0 5202 1000G -RZ not_used not_used -4
note that ports in the file must match ports in the script's variable IPERF_SRV

for scitags_xrootd.sh the file would look something like this:
<user> <dtn_ssh_ip> 0 0 0 0 <showfloor_dtn_ip6> /data/disk0/300.img?scitag.flow=259 -4
```


## Prometheus
Flowd works as Prometheus exporter, which can be accessed on port 9000 (when flowd is running). Port is configurable via PROMETHEUS_SRV_PORT in flowd config.
It will export all network flows (visible to flowd) together with full netlink information, grouped by experiment and activity IDs.

## Grafana
Grafana dashboard can be connected to the Prometheus server to plot details on the network flows produced by the flowd exporter.














