#!/bin/bash
# SuperComputing Scitags Demo
# Author: Marian Babik 

# path to ssh key (to access all remote DTNs)
SSHKEY=<ssh_key_path>
DTN_FILE=$1
# SC showfloor DTN IP
SC_DTN_IP=<dtn_ip>
# Number of iperf servers to start (and ports)
IPERF_SRV='5201 5202 5203 5204 5205 5206 5207 5208 5209 5210 5211 5212 5213 5214 5215 5216 5217 5218 5219 5220 5221 5222 5223 5224 5225 5226 5227 5228 5229 5230 5231 5232 5233 5234 5235 5236 5237 5238 5239 5240 5241 5242 5243 5244 5245'
IPERF_TIMEOUT=15
XRDCP_TIMEOUT=15

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

function run_iperf3 () {
    echo "Running iperf3s ..."
    while read -r DTN_USER DTN_HOST IPERF_LIMIT IPERF_PORT IPERF_SIZE IPERF_FLAGS XROOT_LIMIT XROOT_PATH SSH_EXTRA_ARGS; do
        timeout ${IPERF_TIMEOUT}m ssh $SSH_EXTRA_ARGS $DTN_USER@$DTN_HOST "iperf3 --fq-rate=10G $IPERF_FLAGS -b $IPERF_LIMIT -n $IPERF_SIZE -i 1 -c $XROOT_LIMIT -p $IPERF_PORT" > out/$DTN_HOST.out 2>&1 &
        ssh_pids[${i}]=$!
    done < $DTN_FILE
    wait 15
    echo "Waiting for iperfs to finish ..."
    wait "${ssh_pids[@]}"
}

function run_xrdcp () {
    echo "Running xrdcps ..."
    while read -r DTN_USER DTN_HOST IPERF_LIMIT IPERF_PORT IPERF_SIZE IPERF_FLAGS XROOT_LIMIT XROOT_PATH SSH_EXTRA_ARGS; do
        timeout ${XRDCP_TIMEOUT}m ssh $SSH_EXTRA_ARGS $DTN_USER@$DTN_HOST "xrdcp -d1 -fv -X $XROOT_LIMIT xroot://[$SC_DTN_IP]:1094/$XROOT_PATH /dev/null" > out/$DTN_HOST.out 2>&1 &
    done < $DTN_FILE

    echo "Waiting for xrdcps to finish ..."
    wait
}

mkdir -p out

session_time=$(date +%M)

sudo systemctl -q stop flowd@xrootd
sudo systemctl -q stop flowd@iperf

while true
do
    sudo systemctl -q stop flowd@xrootd
    echo "Starting iperf servers ..."
    pids=()

    for iperf in ${IPERF_SRV}; do
	echo "nohup iperf3 -s -p $iperf >/tmp/iperf3_$iperf.out 2>&1 &"
        nohup iperf3 -s -p $iperf >/tmp/iperf3_$iperf.out 2>&1 &
        pids+=("$!")
    done
    echo "iperf pids: ${pids[@]}"

    echo "Starting flowd@iperf ..."
    sudo systemctl start flowd@iperf
    sudo systemctl status flowd@iperf
    sleep 15
    echo "Session start ..."

    run_iperf3
    session_time=$(date +%M)
    echo ".. session done, killing iperfs"
    for i in "${pids[@]}"
    do
        kill -9 $i
	rm -f /tmp/iperf3_$i.out
    done
    echo "done"
done